<?php
namespace GorillaHub\FilesBundle;

use \GorillaHub\FilesBundle\Exceptions\FileNotFoundException;
use \GorillaHub\FilesBundle\Exceptions\FileException;

class MemoryFileSystem extends AbstractFileSystem
{
	/**
	 * @var int[] An array where, for each element, the key is the full path of a directory, without terminating
	 * 		slash, and the value is the mode (permissions) of that directory.
	 */
	private $directories = array();

	/**
	 * @var ActualMemoryFile[] An array where, for each element, the key is the full path of a file, and the value is
	 * 		an object of type ActualMemoryFile.
	 */
	private $files = array();

	public function createDirectory($directory, $permissions = 0777) {
		Paths::removeTrailingSlash($directory);
		if (isset($this->directories[$directory])) {
			throw new FileException("Directory already exists: " . $directory);
		}
		if (isset($this->files[$directory])) {
			throw new FileException("Tried to create a directory where a file exists: " . $directory);
		}
		$this->directories[$directory] = $permissions;
	}

	public function getFileSize($path) {
		if (!isset($this->files[$path])) {
			throw new FileNotFoundException($path);
		}
		return strlen($this->files[$path]->data);
	}

	public function isFile($path) {
		return isset($this->files[$path]);
	}

	public function chmod($path, $mode) {
		if (isset($this->files[$path])) {
			$this->files[$path]->mode = $mode;
		} else if (isset($this->directories[$path])) {
			$this->directories[$path] = $mode;
		} else {
			throw new FileNotFoundException($path);
		}
	}

	public function filePerms($path) {
		if (isset($this->files[$path])) {
			return $this->files[$path]->mode;
		} else if (isset($this->directories[$path])) {
			return $this->directories[$path];
		} else {
			throw new FileNotFoundException($path);
		}
	}

	public function filePutContents($path, $data, $flags = 0) {
		if (isset($this->directories[$path])) {
			throw new FileException("Tried to create a file where a directory exists: " . $path);
		}
		$dirPath = Paths::removeTrailingSlash(dirname($path));
		if (!isset($this->directories[$dirPath])) {
			throw new FileException("Tried to create a file in a nonexistent directory: " . $dirPath);
		}
		$sizeWritten = strlen($data);
		if (($flags & FILE_APPEND) && isset($this->files[$path])) {
			$this->files[$path]->data .= $data;
		} else {
			$this->files[$path] = new ActualMemoryFile($data, $path);
		}
		return $sizeWritten;
	}

	public function fileGetContents(
		$path,
		$useIncludePath = false,
		$context = null,
		$offset = -1,
		$maxLen = null
	) {
		if (!isset($this->files[$path])) {
			throw new FileNotFoundException($path);
		}
		$data = $this->files[$path]->data;
		$offset = ($offset !== null && $offset > 0) ? (int)$offset : 0;
		if ($maxLen !== null) {
			return substr($data, $offset, $maxLen);
		} else {
			return $offset ? substr($data, $offset) : $data;
		}
	}

	public function createSafeName($name) {
		return urlencode($name);
	}

}