<?php

namespace GorillaHub\FilesBundle\Domain;

/**
 * Class File
 * @package GorillaHub\FilesBundle
 */
class File extends Node
{

    /**
     * @var int
     */
    private $size = 0;
    /**
     * @var string
     */
    private $content = '';

    /**
     * @param string $path
     */
    public function __construct($path = null)
    {
        $this->setPath($path);
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     *
     * @return $this
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     *
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

}