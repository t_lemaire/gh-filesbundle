<?php

namespace GorillaHub\FilesBundle\Domain;

/**
 * Class Path
 * @package GorillaHub\FilesBundle
 */
class Path
{
	/**
	 * @var string
	 */
	private $path = '';

	/**
	 * @param string $path
	 *
	 * @return self
	 */
	public function setPath($path)
	{
		$this->path = $path;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		return $this->path;
	}

} 