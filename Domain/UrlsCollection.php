<?php

namespace GorillaHub\FilesBundle\Domain;

use \GorillaHub\FilesBundle\Exceptions\DuplicatedUrlException;

class UrlsCollection implements \IteratorAggregate, \Countable
{

    private $urls = array();

    /**
     * @return \ArrayIterator|\Traversable
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->urls);
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->urls);
    }

	/**
	 * @param Url  $url
	 * @param bool $overwrite
	 *
	 * @return $this
	 * @throws \GorillaHub\FilesBundle\Exceptions\DuplicatedUrlException
	 */
	public function add(Url $url, $overwrite = false)
    {
		$md5 = md5((string)$url);

		if (false === $overwrite) {
			if (isset($this->urls[$md5])) {
				throw new DuplicatedUrlException('The Url already exists in the collection.');
			}
		}

		$this->urls[$md5] = $url;

        return $this;
    }

	/**
	 * @param $md5
	 *
	 * @return null
	 */
	public function get($md5)
	{
		if (isset($this->urls[$md5])) {
			return $this->urls[$md5];
		}

		return null;
	}

	/**
	 * @param array $urls
	 */
	public function setUrls(array $urls = array())
	{
		foreach ($urls as $url) {
			$this->add($url, true);
		}
	}

	/**
	 * @return File[]
	 */
	public function getUrls()
	{
		return $this->urls;
	}

}