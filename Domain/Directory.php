<?php

namespace GorillaHub\FilesBundle\Domain;

/**
 * Class Directory
 * @package GorillaHub\FilesBundle
 */
class Directory extends Node implements \Countable, \IteratorAggregate
{

    /**
     * @var Node[]
     */
    private $containedNodes;

    public function __construct()
    {
        $this->containedNodes = array();
    }

    /**
     * @return \ArrayIterator|\Traversable
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->containedNodes);
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->containedNodes);
    }

    /**
     * @param Node $node
     */
    public function addNode(Node $node)
    {
        $this->containedNodes[(string)$node->getPath()] = $node;
    }

    /**
     * @param $nodePath
     *
     * @return Node|null
     */
    public function getNode($nodePath)
    {
        if (isset($this->containedNodes[$nodePath])) {
            return $this->containedNodes[$nodePath];
        }

        return null;
    }

    /**
     * @param Node[] $nodes
     */
    public function setNodes(array $nodes)
    {
        /** @var Node $node */
        foreach ($nodes as $path => $node) {
            if (is_string($path) && $node instanceof Node) {
                $this->containedNodes[$path] = $node;
            }
        }
    }

    /**
     * @return Node[]
     */
    public function getNodes()
    {
        return $this->containedNodes;
    }

}