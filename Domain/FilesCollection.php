<?php

namespace GorillaHub\FilesBundle\Domain;

use \GorillaHub\FilesBundle\Exceptions\DuplicatedFilePathException;
use \GorillaHub\FilesBundle\Exceptions\FileIsMissingPathDefinitionException;

/**
 * Class FilesCollection
 * @package GorillaHub\SDKs\OriginPullBundle\Domain\Calls\Jobs
 */
class FilesCollection
{
	/**
	 * @var File[]
	 */
	protected $files = array();

	/**
	 * @return \ArrayIterator|\Traversable
	 */
	public function getIterator()
	{
		return new \ArrayIterator($this->files);
	}

	/**
	 * @return int
	 */
	public function count()
	{
		return count($this->files);
	}

	/**
	 * @param File $file
	 * @param bool $overwrite
	 *
	 * @return $this
	 * @throws \GorillaHub\FilesBundle\Exceptions\DuplicatedFilePathException
	 * @throws \GorillaHub\FilesBundle\Exceptions\FileIsMissingPathDefinitionException
	 */
	public function add(File $file, $overwrite = false)
	{
		$filePath = (string)$file->getPath();

		if (null === $filePath || '' === $filePath) {
			throw new FileIsMissingPathDefinitionException('Unable to add the File to the collection, it is missing Path definition.');
		}

		if (false === $overwrite) {
			if (isset($this->files[$filePath])) {
				throw new DuplicatedFilePathException('The File Path already exists in the collection.');
			}
		}

		$this->files[$filePath] = $file;

		return $this;
	}

	/**
	 * @param $filePath
	 *
	 * @return File|null
	 */
	public function get($filePath)
	{
		if (isset($this->files[$filePath])) {
			return $this->files[$filePath];
		}

		return null;
	}

	/**
	 * @param array $files
	 */
	public function setFiles(array $files = array())
	{
		foreach ($files as $file) {
			$this->add($file, true);
		}
	}

	/**
	 * @return File[]
	 */
	public function getFiles()
	{
		return $this->files;
	}

} 