<?php

namespace GorillaHub\FilesBundle\Domain;

use GorillaHub\FilesBundle\Paths;

class Url
{

	/**
	 * @var string
	 */
	private $scheme = '';

	/**
	 * @var string
	 */
	private $hostName = '';

	/**
	 * @var string
	 */
	private $port = 0;

	/**
	 * @var string
	 */
	private $user = '';

	/**
	 * @var string
	 */
	private $password = '';

	/**
	 * @var string
	 */
	private $path = '';

	/**
	 * @var string
	 */
	private $query = '';

	/**
	 * @var string
	 */
	private $fragment = '';

	/**
	 * @param $url
	 *
	 * @internal param string $pathFile
	 */
	public function __construct($url = null)
	{
		if (null !== $url) {
			$parts = parse_url($url);

			if (isset($parts['scheme'])) {
				$this->setScheme($parts['scheme']);
			}

			if (isset($parts['host'])) {
				$this->setHostName($parts['host']);
			}

			if (isset($parts['user'])) {
				$this->setUser($parts['user']);
			}

			if (isset($parts['pass'])) {
				$this->setPassword($parts['pass']);
			}

			if (isset($parts['path'])) {
				$this->setPath($parts['path']);
			}

			if (isset($parts['query'])) {
				$this->setQuery($parts['query']);
			}

			if (isset($parts['fragment'])) {
				$this->setFragment($parts['fragment']);
			}

			if (isset($parts['port'])) {
				$this->setPort($parts['port']);
			}
		}
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		$credentials = '';

		if ($this->user !== '') {
			$credentials = $this->user . ':' . $this->password . '@';
		}

		$scheme = '';

		if ($this->scheme !== '') {
			$scheme = $this->scheme . '://';
		}

		$port = '';

		if ($this->port !== 0) {
			$port = ':' . $this->port;
		}

		$query = '';

		if ($this->query !== '') {
			$query = '?' . $this->query;
		}

		$fragment = '';

		if ($this->fragment !== '') {
			$query = '#' . $this->fragment;
		}

		return $scheme . $credentials . $this->hostName . $port . $this->path . $query . $fragment;
	}


	/**
	 * @param string $fragment
	 *
	 * @return $this
	 */
	final public function setFragment($fragment)
	{
		$this->fragment = $fragment;

		return $this;
	}

	/**
	 * @return string
	 */
	final public function getFragment()
	{
		return $this->fragment;
	}

	/**
	 * @param string $hostName
	 *
	 * @return $this
	 */
	final public function setHostName($hostName)
	{
		$this->hostName = $hostName;

		return $this;
	}

	/**
	 * @return string
	 */
	final public function getHostName()
	{
		return $this->hostName;
	}

	/**
	 * @param string $password
	 *
	 * @return $this
	 */
	final public function setPassword($password)
	{
		$this->password = $password;

		return $this;
	}

	/**
	 * @return string
	 */
	final public function getPassword()
	{
		return $this->password;
	}

	/**
	 * @param string $path
	 *
	 * @return $this
	 */
	final public function setPath($path)
	{
		$this->path = Paths::cleanPath($path);

		return $this;
	}

	/**
	 * @return string
	 */
	final public function getPath()
	{
		return $this->path;
	}


	/**
	 * @param string $port
	 *
	 * @return $this
	 */
	final public function setPort($port)
	{
		$this->port = (int)$port;

		return $this;
	}

	/**
	 * @return string
	 */
	final public function getPort()
	{
		return $this->port;
	}

	/**
	 * @param string $query
	 *
	 * @return $this
	 */
	final public function setQuery($query)
	{
		$this->query = $query;

		return $this;
	}

	/**
	 * @return string
	 */
	final public function getQuery()
	{
		return $this->query;
	}

	/**
	 * @param string $scheme
	 *
	 * @return $this
	 */
	final public function setScheme($scheme)
	{
		$this->scheme = $scheme;

		return $this;
	}

	/**
	 * @return string
	 */
	final public function getScheme()
	{
		return $this->scheme;
	}

	/**
	 * @param string $user
	 *
	 * @return $this
	 */
	final public function setUser($user)
	{
		$this->user = $user;

		return $this;
	}

	/**
	 * @return string
	 */
	final public function getUser()
	{
		return $this->user;
	}

}