<?php

namespace GorillaHub\FilesBundle\Domain;

/**
 * An instance of this class identifies a directory or a file.  If the specific type is known, you should instantiate
 * File or Directory.
 */
class Node
{
	/**
	 * @var string
	 */
	private $path;
	/**
	 * @var int
	 */
	private $permissions = 0;
	/**
	 * @var string
	 */
	private $owner = '';
	/**
	 * @var string
	 */
	private $ownerGroup = '';
	/**
	 * @var int
	 */
	private $createdDate = 0;
	/**
	 * @var int
	 */
	private $lastModificationDate = 0;

    /** @var  int */
    private $client_id;
    /**
     * @var string Alias name of the client storage volume
     */
    private $volumeName = 'default';

    /**
     * @return string
     */
    public function getVolumeName()
    {
        return $this->volumeName !== null ? $this->volumeName : 'default';
    }

    /**
     * @param string $volumeName
     * @return Node
     */
    public function setVolumeName($volumeName)
    {
        $this->volumeName = $volumeName;
        return $this;
    }
	/**
	 * @param string $path
	 *
	 * @return $this
	 */
	public function setPath($path)
	{
		$this->path = $path;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}

	/**
	 * @param int $permissions
	 *
	 * @return $this
	 */
	public function setPermissions($permissions)
	{
		$this->permissions = $permissions;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getPermissions()
	{
		return $this->permissions;
	}

	/**
	 * @param string $owner
	 *
	 * @return $this
	 */
	public function setOwner($owner)
	{
		$this->owner = $owner;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getOwner()
	{
		return $this->owner;
	}

	/**
	 * @param string $ownerGroup
	 *
	 * @return $this
	 */
	public function setOwnerGroup($ownerGroup)
	{
		$this->ownerGroup = $ownerGroup;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getOwnerGroup()
	{
		return $this->ownerGroup;
	}

	/**
	 * @param int $createdDate
	 *
	 * @return $this
	 */
	public function setCreatedDate($createdDate)
	{
		$this->createdDate = $createdDate;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getCreatedDate()
	{
		return $this->createdDate;
	}

	/**
	 * @param int $lastModificationDate
	 *
	 * @return $this
	 */
	public function setLastModificationDate($lastModificationDate)
	{
		$this->lastModificationDate = $lastModificationDate;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getLastModificationDate()
	{
		return $this->lastModificationDate;
	}

    /**
     * @return int
     */
    public function getClientId()
    {
        return $this->client_id;
    }

    /**
     * @param int $client_id
     */
    public function setClientId($client_id)
    {
        $this->client_id = $client_id;
    }

}
