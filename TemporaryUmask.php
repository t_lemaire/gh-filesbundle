<?php
namespace GorillaHub\FilesBundle;

use \GorillaHub\GeneralBundle\LocalLock;

/**
 * @package GorillaHub/FilesBundle
 */
class TemporaryUmask
{
	/** @var LocalLock */
	private $_localLock;

	/** @var int */
	private $_oldUmask;

	/**
	 * @param int $umask The umask that should be in effect while this object exists.  @see umask().
	 */
	public function __construct($umask) {
		$this->_localLock = new LocalLock(__CLASS__);
		$this->_oldUmask = umask($umask);
	}

	public function __destruct() {
		umask($this->_oldUmask);
	}
};