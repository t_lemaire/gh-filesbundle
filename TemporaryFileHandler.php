<?php
namespace GorillaHub\FilesBundle;

use \GorillaHub\FilesBundle\Exceptions\FileException;
use \GorillaHub\FilesBundle\Exceptions\FileNotFoundException;

/**
 * This is a version of FileHandler that automatically creates the file on construct and deletes the file on destruct,
 * unless commit() is called.
 */
class TemporaryFileHandler extends FileHandler
{
	/** @var bool */
	private $toBeDeleted = true;

	/**
	 * @param string $fileName The name of the file to open.
	 * @param int|null $permissions The permissions for a new file, if a new file is created, or null if the
	 * 		permissions should be determined by the umask.
	 * @throws FileNotFoundException if the file does not exist and was not to be created.
	 * @throws FileException if the file cannot be opened.
	 */
	public function __construct($fileName, $permissions = null) {
		parent::__construct($fileName, "wb", $permissions);
	}

	public function __destruct() {
		$filename = $this->getFilename();
		parent::__destruct();
		if ($this->toBeDeleted) {
			unlink($filename);
		}
	}

	/**
	 * If this is called, the file is not deleted when the object is destructed.
	 */
	public function commit() {
		$this->toBeDeleted = false;
	}


}