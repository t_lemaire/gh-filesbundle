<?php

namespace GorillaHub\FilesBundle;

use \GorillaHub\GeneralBundle\Strings;
use \GorillaHub\GeneralBundle\Php;

/**
 * @package GorillaHub/FilesBundle
 */
class Paths
{
	/**
	 * This method removes consecutive slashes and single-dot directories and applies double-dot directories
	 * to the path where possible.  Double-dots that try to go above the directory to which $path is relative or
	 * which try to go above the root directory are removed.
	 *
	 * @param string $path The relative or absolute path to clean.
	 *
	 * @return string The cleaned path.
	 */
	static public function cleanPath($path)
	{
		$outParts = array();
		$inParts  = explode('/', $path);
		foreach ($inParts as $inPart) {
			if ($inPart !== '' && $inPart !== '.') {
				if ($inPart === '..') {
					array_pop($outParts);
				} else {
					$outParts[] = $inPart;
				}
			}
		}
		if (empty($outParts)) {
			if (substr($path, 0, 1) === '/') {
				return '/';
			} else {
				return '';
			}
		} else {
			$outPath = (substr($path, 0, 1) === '/') ? '/' : '';
			$outPath .= implode('/', $outParts);

			return (substr($path, -1) === '/')
				? ($outPath . '/')
				: $outPath;
		}
	}

	/**
	 * @param string $path A path, with or without a trailing slash.
	 *
	 * @return string The value of $path, with the trailing slash removed, if there was one.
	 */
	static public function removeTrailingSlash($path)
	{
		return rtrim($path, '/');
	}

	/**
	 * @param string $path A path, with or without a trailing slash.
	 *
	 * @return string The value of $path, with a trailing slash added, if there wasn't already one.
	 */
	static public function addTrailingSlash($path)
	{
		return rtrim($path, '/') . '/';
	}

	/**
	 * @param string $path A path, with or without a leading slash.
	 *
	 * @return string The value of $path, with the leading slash removed, if there was one.
	 */
	static public function removeLeadingSlash($path)
	{
		return ltrim($path, '/');
	}

	/**
	 * @param string $path A path, with or without a leading slash.
	 *
	 * @return string The value of $path, with a leading slash added, if there wasn't already one.
	 */
	static public function addLeadingSlash($path)
	{
		return '/'.ltrim($path, '/');
	}

	/**
	 * This returns a path which is $path2 within $path1.  $path2 is interpreted as a relative
	 * path, regardless of whether it begins with a slash (i.e. the slash is ignored).  $path1 may
	 * be an absolute path or a relative path (an empty string is interpreted as a relative path).
	 * $path1 may be specified with or without a terminating slash.  The returned path is absolute
	 * if $path1 is absolute, or relative if $path1 is relative.  If both input paths are empty, the
	 * return value is also empty.
	 *
	 * If this function is called with more than two arguments, then all of the paths are joined
	 * from left to right.
	 */
	static public function joinPaths($path1, $path2)
	{

		$numberOfArgs = func_num_args();

		if ($numberOfArgs > 2) {
			if ($numberOfArgs === 3) {
				$lastArg = func_get_arg(2);
				$path2   = Paths::joinPaths($path2, $lastArg);
			} else {
				$allArgs   = func_get_args();
				$newArgs   = array_slice($allArgs, 0, $numberOfArgs - 2);
				$newArgs[] = Paths::joinPaths(
					$allArgs[$numberOfArgs - 2],
					$allArgs[$numberOfArgs - 1]
				);

				return call_user_func_array(array(__CLASS__, 'joinPaths'), $newArgs);
			}
		}

		if ($path1 !== '') {
			if (substr($path1, -1, 1) !== '/') {
				$path1 .= '/';
			}
		}
		if (substr($path2, 0, 1) === '/') {
			$path2 = (string)substr($path2, 1);
		}

		return $path1 . $path2;
	}

	/**
	 * This function changes a path that exists within some root directory to exist within
	 * another root directory.  For example, replaceRootPath('/a/b/c/d.txt', '/a/b', '/e/f')
	 * returns '/e/f/c/d.txt'.  It does not matter whether $oldRoot and $newRoot are terminated
	 * by slashes.
	 *
	 * @param string $fullPath Path to modify.
	 * @param string $oldRoot  Root of $fullPath.  $fullPath must begin with this string.
	 * @param string $newRoot  New root of $fullPath.
	 *
	 * @return string Modified $fullPath.
	 * @throws \Exception if $fullPath does not begin with $oldRoot.
	 */
	static public function replacePathRoot($fullPath, $oldRoot, $newRoot)
	{
		if ($oldRoot !== '/') {
			$oldRoot = Paths::removeTrailingSlash($oldRoot);
		}
		$relativePath = Strings::removePrefix($fullPath, $oldRoot);
		$lastOldChar = substr($oldRoot, -1);
		if ($lastOldChar !== '' && $lastOldChar !== '/') {
			$firstNewChar = substr($relativePath, 0, 1);
			if ($firstNewChar !== '' && $firstNewChar !== '/') {
				throw new \InvalidArgumentException(
					"Path doesn't start with the old root."
				);
			}
		}

		return $relativePath === '' ? $newRoot : Paths::joinPaths($newRoot, $relativePath);
	}

	/**
	 * @param string $path A path of a file from which to get the extension.
	 *
	 * @return string|null The extension of the file specified in the path, or null if none
	 */
	static public function getExtensionFromPath($path)
	{
		$pathInfo = pathinfo($path);

		if (isset($pathInfo['extension'])) {
			return $pathInfo['extension'];
		}

		return null;
	}

	/**
	 * @param string $path A path that may begin with tilde.
	 *
	 * @return string The specified path with the tilde expanded to the current home directory.
	 */
	static public function expandTildeToHome($path)
	{
		if (preg_match('`^~(\\/.*)?$`', $path, $matches)) {
			return (isset($matches[1]) && $matches[1] !== '')
				? self::joinPaths(getenv('HOME'), $matches[1])
				: getenv('HOME');
		}

		return $path;
	}

	/**
	 * @param string $path
	 * @return int The number of nodes referenced in the path.  Examples:
	 * 		'/' ⟼ 0, '' ⟼ 0, 'a' ⟼ 1, '/a' ⟼ 1, 'a/b' ⟼ 2.
	 */
	static public function getPathDepth($path) {
		$path = Paths::cleanPath($path);
		$path = trim($path, '/');
		if ($path === '' || $path === '/') {
			return 0;
		}
		return substr_count($path, '/') + 1;
	}

	/**
	 * @param string $path
	 * @return bool True iff the specified path is an absolute path (begins with a slash).
	 */
	static public function isPathAbsolute($path) {
		return substr($path, 0, 1) === '/';
	}

	/**
	 * This checks, in a deterministic way, if two paths are the same.  Trailing slashes are ignored.
	 * The disk is not accessed, so the existence of the files is not checked.  This function does not depend on
	 * the current working directory, so a relative path is always different from an absolute path.
	 *
	 * @param string $path1
	 * @param string $path2
	 * @return bool True iff the two paths refer to the same file or directory.
	 */
	static public function arePathsSame($path1, $path2) {
		$normalized1 = Paths::removeTrailingSlash(Paths::cleanPath($path1));
		$normalized2 = Paths::removeTrailingSlash(Paths::cleanPath($path2));
		return $normalized1 === $normalized2;
	}

	/**
	 * This checks, in a deterministic way, if $path is a descendent of $directory.  Trailing slashes are ignored.
	 * The disk is not accessed, so the existence of the files is not checked.  This function does not depend on
	 * the current working directory, so a relative path is always unrelated to an absolute path.  Note that a
	 * directory is not considered to be "below" itself.
	 *
	 * @param string $directory
	 * @param string $path
	 * @return bool True iff $path is some file or directory within $directory.
	 */
	static public function isPathBelowDirectory($directory, $path) {
		try {
			$truncated = Paths::replacePathRoot($path, $directory, '/');
			return $truncated !== '' && $truncated !== '/';
		} catch (\Exception $e) {
		}
		return false;
	}

	/**
	 * This checks, in a deterministic way, if $path is a equal to or a descendent of $directory.  Trailing slashes
	 * are ignored.  The disk is not accessed, so the existence of the files is not checked.  This function does not
	 * depend on the current working directory, so a relative path is always unrelated to an absolute path.
	 *
	 * @param string $directory
	 * @param string $path
	 * @return bool True iff $path is equal to $directory or is some file or directory within $directory.
	 */
	static public function isPathAtOrBelowDirectory($directory, $path) {
		return Paths::isPathBelowDirectory($directory, $path)
				|| Paths::arePathsSame($directory, $path);
	}

	/**
	 * @param string $path1
	 * @param string $path2
	 * @return bool True iff $path1 is equal to $path2, or if one path refers to an object within the other path.
	 *	  The paths are not checked for validity.  The paths are unrelated if one is absolute and the other is
	 *	  relative.
	 */
	static public function arePathsRelated($path1, $path2) {
		if (Paths::isPathAbsolute($path1) !== Paths::isPathAbsolute($path2)) {
			return false;
		}
		if (Paths::arePathsSame($path1, $path2)) {
			return true;
		}
		return Paths::isPathBelowDirectory($path1, $path2)
				|| Paths::isPathBelowDirectory($path2, $path1);
	}

	/**
	 * @param string $path
	 * @return string[] All ancestors of the specified path.
	 */
	static public function getAncestorPaths($path) {
		$ancestorPaths = [];
		for (;;) {
			$path = Paths::getParentPath($path);
			if ($path === null) {
				break;
			}
			$ancestorPaths[] = $path;
		}
		return $ancestorPaths;
	}

	/**
	 * @param string $path
	 * @return string|null The parent of the specified path, or null if the path is the root.
	 */
	static public function getParentPath($path) {
		$path = Paths::removeTrailingSlash($path);
		if ($path === '') {
			return null;
		}
		$parent = preg_replace('`/[^/]+$`', '', $path);
		if ($parent === $path) {
			return '';
		}
		return $parent !== '' ? $parent : '/';
	}

}
