<?php
namespace GorillaHub\FilesBundle;

use \GorillaHub\FilesBundle\Exceptions\FileException;

/**
 * @package GorillaHub/FilesBundle
 */
abstract class AbstractFileHandler
{
	/**
	 * This method moves the read/write cursor to the specified position.
	 *
	 * @param int $offset The new position of the read/write cursor, relative to the position specified in $whence.
	 * @param int $whence The position from which $offset is specified:
	 * 		SEEK_SET:  The beginning of the file, i.e. zero.
	 * 		SEEK_CUR:  The current cursor position, i.e. the value that would be returned by tell().
	 * 		SEEK_END:  The end of the file, i.e. the value that would be returned by getSize().
	 * @throws FileException if the cursor could not be updated to the specified position.
	 */
	abstract public function seek($offset, $whence = SEEK_SET);

	/**
	 * @return int The current position of the read/write cursor, relative to the beginning of the file.
	 */
	abstract public function tell();

	/**
	 * This method reads the file from the read/write cursor up to the specified number of bytes.  The number of
	 * bytes read may be less than requested (see fread()).  The cursor is moved to point to the first byte following
	 * the bytes that were read.
	 *
	 * @param int $length The maximum number of bytes to read.
	 * @return string The data that was read.
	 * @throws FileException if the file could not be read.
	 */
	abstract public function read($length);

	/**
	 * This method reads the specified number of bytes from the file, starting at the read/write cursor.  The cursor
	 * is moved to point to the first byte following the bytes that were read.
	 *
	 * @param int $length The number of bytes to read.
	 * @return string The data that was read.
	 * @throws FileException if not all requested bytes could be read.
	 */
	abstract public function readExactly($length);

	/**
	 * This method reads the file from the read/write cursor until the end of the file, and moves the cursor to
	 * the end of the file.
	 *
	 * @return string All data from the file from the cursor to the end of the file.
	 * @throws FileException if the file could not be read.
	 */
	abstract public function readToEnd();

	/**
	 * This method reads the entire file, from beginning to end.  The read/write cursor is moved to the end of the
	 * file.
	 *
	 * @return string The entire contents of the file.
	 * @throws FileException if the file could not be read.
	 */
	abstract public function readAll();

	/**
	 * This method writes as much as possible of the specified data to the file, at the read/write cursor.  The
	 * cursor is moved to the first byte following the bytes that were written.
	 *
	 * @param string $data The data to be written to the file.
	 * @return int The number of bytes actually written.
	 * @throws FileException if the file could not be written to.
	 */
	abstract public function write($data);

	/**
	 * This method writes all of the specified data to the file, at the read/write cursor.  The
	 * cursor is moved to the first byte following the bytes that were written.
	 *
	 * @param string $data The data to be written to the file.
	 * @throws FileException if not all bytes could be written.
	 */
	abstract public function writeAll($data);

	/**
	 * This function behaves like read(), except that it does not return the data.  Unlike seek(), this method
	 * determines the number of bytes that would have been read if read() had been called.
	 *
	 * @param int $size The maximum number of bytes to skip.
	 * @return int The number of bytes that were skipped.
	 */
	abstract public function skip($size);

	/**
	 * This function behaves like readExactly(), except that it does not return the data.  Unlike seek(), this method
	 * throws an exception if the specified number of bytes could not be skipped.
	 *
	 * @param int $size The number of bytes to skip.
	 * @throws FileException if not all requested bytes could be skipped.
	 */
	abstract public function skipExactly($size);

	/**
	 * This method creates an advisory lock on the file.
	 *
	 * @param int $operation See flock().
	 * @param int|null $wouldblock See flock().
	 * @param int $timeout The number of microseconds before acquiring the lock should fail, or zero if
	 * 		only one non-blocking attempt should be made, or false if it should attempt forever.
	 * @throws FileException if the file could not be locked.
	 */
	abstract public function lock($operation, &$wouldblock = null, $timeout);

	/**
	 * This method releases an advisory lock on the file.
	 *
	 * @param int|null $wouldblock See flock().
	 * @throws FileException if the file could not be unlocked.
	 */
	abstract public function unlock(&$wouldblock = null);

	/**
	 * This method truncates the file to the specified size.
	 *
	 * @param int $size The new size of the file.
	 * @throws FileException if the file could not be truncated.
	 */
	abstract public function truncate($size);

	/**
	 * @return int The size of the file, in bytes.
	 * @throws FileException if the size could not be determined.
	 */
	abstract public function getSize();

	/**
	 * @return string The filename of the file.
	 */
	abstract public function getFilename();

	/**
	 * This method reads and echoes the rest of the file, beginning at the read/write cursor.  The cursor is moved
	 * to the end of the file.
	 *
	 * @throws FileException if the data could not be read.
	 */
	abstract public function passThru();

	/**
	 * This method writes as many as possible of the specified byte to the file, at the read/write cursor.  The
	 * cursor is moved to the first byte following the bytes that were written.
	 *
	 * @param int $byte The byte to be written, in the range [0, 255].
	 * @param int $numberOfBytes The number of bytes to write.
	 * @return int The number of bytes actually written.
	 * @throws FileException if the file could not be written to.
	 */
	abstract public function writeRepeatedByte($byte, $numberOfBytes);

	/**
	 * This method writes the specified number of the specified byte to the file, at the read/write cursor.  The
	 * cursor is moved to the first byte following the bytes that were written.
	 *
	 * @param int $byte The byte to be written, in the range [0, 255].
	 * @param int $numberOfBytes The number of bytes to write.
	 * @throws FileException if not all bytes could be written.
	 */
	abstract public function writeAllRepeatedBytes($byte, $numberOfBytes);

	/**
	 * This method ensures that all data that was queued to be written has been written.  See fflush().
	 *
	 * @throws FileException if the file could not be flushed.
	 */
	abstract public function flush();

	/**
	 * @return int The unix timestamp at which the file was last modified.
	 * @throws FileException if the modify time could not be determined.
	 */
	abstract public function getModifyTime();

	/**
	 * This method unlinks the file.
	 *
	 * @throws FileException if the file could not be unlinked.
	 */
	abstract public function unlink();

	/**
	 * This method resolves the real path of the file.  See realpath().
	 *
	 * @return string The real path of the file.
	 * @throws FileException if the real path could not be resolved.
	 */
	abstract public function getRealPath();

	/**
	 * @param int $mode The new file mode.  See chmod().
	 * @throws FileException if the file mode could not be set.
	 */
	abstract public function chmod($mode);

	/**
	 * @return int The file permissions of the file.  See fileperms().
	 * @throws FileException if the file permissions could not be determined.
	 */
	abstract public function fileperms();

	/**
	 * @return array Information about the file.  See fstat().
	 * @throws FileException if the file information could not be determined.
	 */
	abstract public function stat();

	/**
	 * @return bool True if the read/write cursor is at or past the end of the file, false otherwise.
	 */
	public function isAtEnd() {
		return $this->tell() >= $this->getSize();
	}
}