<?php
namespace GorillaHub\FilesBundle;

use \GorillaHub\FilesBundle\Exceptions\FileException;
use \GorillaHub\GeneralBundle\LocalLock;

/**
 * @package GorillaHub/FilesBundle
 */
class MemoryFileHandler extends AbstractFileHandler
{
	/** @var ActualMemoryFile The actual memory file that this handle operates on. */
	private $_actual;

	/** @var int The position of the read/write cursor. */
	private $_position = 0;

	/** @var LocalLock|null */
	private $_localLock = null;

	public function __construct(ActualMemoryFile $data) {
		$this->_actual = $data;
	}

	public function seek($position, $whence = SEEK_SET) {
		switch ($whence) {
			case SEEK_SET:
				$this->_position = 0;
				break;
			case SEEK_END:
				$this->_position = strlen($this->_actual->data);
				break;
			case SEEK_CUR:
				break;
			default:
				throw new FileException("Unsupported value of \$whence passed to seek(): $whence");
		}
		$this->_position += $position;
	}

	public function tell() {
		return $this->_position;
	}

	public function read($size) {
		$substring = (string)substr($this->_actual->data, $this->_position, $size);
		$this->_position += strlen($substring);
		return $substring;
	}

	public function readExactly($size) {
		$data = $this->read($size);
		if (strlen($data) !== $size) {
			throw new FileException("Unexpected end of file.");
		}
		return $data;
	}

	public function readToEnd() {
		$size = $this->getSize();
		return $this->readExactly($size - $this->tell());
	}

	public function readAll() {
		$size = $this->getSize();
		$this->seek(0);
		return $this->readExactly($size);
	}

	public function write($data) {
		$this->_actual->modifyTime = time();
		$this->_actual->data = str_pad($this->_actual->data, $this->_position, "\0");
		for ($i = 0 ; $i < strlen($data) ; $i++) {
			$this->_actual->data[$i + $this->_position] = $data[$i];
		}
		$this->_position += strlen($data);
		return strlen($data);
	}

	public function writeAll($data) {
		$this->write($data);
	}

	public function skip($size) {
		$remaining = strlen($this->_actual->data) - $this->_position;
		$actuallySkipped = min($size, $remaining);
		$this->_position += $actuallySkipped;
		return $actuallySkipped;
	}

	public function skipExactly($size) {
		$actuallySkipped = $this->skip($size);
		if ($actuallySkipped !== $size) {
			throw new FileException("Unexpected end of file.");
		}
		return $size;
	}

	public function lock($operation, &$wouldblock = null, $timeout) {
		try {
			$this->_localLock = new LocalLock(spl_object_hash($this->_actual), $timeout);
		} catch (\Exception $e) {
			throw new FileException("Couldn't locally lock " . $this->_actual->fileName . ".");
		}
	}

	public function unlock(&$wouldblock = null) {
		$this->_localLock = null;
	}

	public function truncate($size) {
		$this->_actual->modifyTime = time();
		$this->_actual->data = (string)substr($this->_actual->data, 0, $size);
	}

	public function getSize() {
		return strlen($this->_actual->data);
	}

	public function getFilename() {
		return $this->_actual->fileName;
	}

	public function passThru() {
		echo $this->readToEnd();
	}

	public function writeRepeatedByte($byte, $numberOfBytes) {
		$string = str_repeat(chr($byte), $numberOfBytes);
		$this->write($string);
	}

	public function writeAllRepeatedBytes($byte, $numberOfBytes) {
		$this->writeRepeatedByte($byte, $numberOfBytes);
	}

	public function flush() {
	}

	public function getModifyTime() {
		return $this->_actual->modifyTime;
	}

	public function unlink() {
		$this->_actual->data = '';
	}

	public function getRealPath() {
		return $this->_actual->fileName;
	}

	public function chmod($mode) {
		$this->_actual->mode = $mode;
	}

	public function fileperms() {
		return $this->_actual->mode;
	}

	public function stat() {
		throw new FileException("stat() is unavailable for memory files.");
	}

}