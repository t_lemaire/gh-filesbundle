<?php
namespace GorillaHub\FilesBundle;

use \GorillaHub\FilesBundle\Exceptions\FileException;
use \GorillaHub\FilesBundle\Exceptions\FileNotFoundException;

/**
 * @package GorillaHub/FilesBundle
 */
abstract class AbstractFileSystem
{
	/**
	* Creates a directory, if it does not already exist, recursively.
	*
	* @param string $directory The name of the directory, with or without a terminating slash.
	* @param int $permissions The permissions (mode) of the new directory.
	* @throws FileException if the folder cannot be created.
	*/
	abstract public function createDirectory($directory, $permissions = 0777);

	/**
	 * @param string $path The full path of the file to measure.
	 * @return int The size of the file, in bytes.
	 * @throws FileException if the file size could not be deterined.
	 * @throws FileNotFoundException if the file does not exist (if the filesystem can distinguish this reason).
	 */
	abstract public function getFileSize($path);

	/**
	 * @param string $path
	 * @return bool True if the path refers to a file, or false otherwise.
	 */
	abstract public function isFile($path);

	/**
	 * Sets the permissions of the specified file or directory.
	 *
	 * @param string $path
	 * @param int $mode The mode {@see chmod()}.
	 * @throws FileException if the file or directory could not be modified.
	 * @throws FileNotFoundException if the file does not exist (if the filesystem can distinguish this reason).
	 */
	abstract public function chmod($path, $mode);

	/**
	 * @param string $path The file to inspect.
	 * @return int The permissions of the file {@see file_perms()}.
	 * @throws FileException if the file or directory could not be modified.
	 * @throws FileNotFoundException if the file does not exist (if the filesystem can distinguish this reason).
	 */
	abstract public function filePerms($path);

	/**
	 * Writes the contents of $data to the specified file.
	 *
	 * @param string $path
	 * @param string $data
	 * @param int $flags A combination of flags; {@see file_put_contents()}.
	 * @return int The number of bytes written.
	 * @throws FileException if the data was not completely written.
	 */
	abstract public function filePutContents($path, $data, $flags = 0);

	/**
	 * @param string $path
	 * @param bool $useIncludePath
	 * @param resource|null $context {@see file_get_contents()}.
	 * @param int|null $offset {@see file_get_contents()}.
	 * @param null $maxLen {@see file_get_contents()}.
	 * @return string The data content of the file.
	 * @throws FileException if the file could not be read.
	 * @throws FileNotFoundException if the file does not exist (if the filesystem can distinguish this reason).
	 */
	abstract public function fileGetContents(
		$path,
		$useIncludePath = false,
		$context = null,
		$offset = -1,
		$maxLen = null
	);

	/**
	 * This method transforms the specified file name so that it has only characters that are legal in a filename and
	 * have no special meaning.  This function is injective:  if any two input names are different, the corresponding
	 * output names will be different.
	 *
	 * @param string $name The desired file name.
	 * @return string The valid file name.
	 */
	abstract public function createSafeName($name);
}