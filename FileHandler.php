<?php
namespace GorillaHub\FilesBundle;

use \GorillaHub\GeneralBundle\LocalLock;
use \GorillaHub\GeneralBundle\Php;
use \GorillaHub\FilesBundle\Exceptions\FileException;
use \GorillaHub\FilesBundle\Exceptions\FileNotFoundException;

/**
 * @package GorillaHub/FilesBundle
 *
 * An object of this class wraps a file resource.  Primarily, it spares the programmer from having to call fclose()
 * and check for errors, because the file resource is closed in the descriptor and the methods throw exceptions to
 * indicate errors instead of returning special return codes.  There are other convenient methods that are not
 * available with the default API.
 */
class FileHandler extends AbstractFileHandler
{
	const LOCAL_LOCK_TIMEOUT = 1000000;

	private $_fileName;
	private $_handle;
	private $_isLocked;
	private $_realPath = null;
	private $_localLock;

	/**
	 * @param string $fileName The name of the file to open.
	 * @param string $mode The opening mode: @see fopen().
	 * @param int|null $permissions The permissions for a new file, if a new file is created, or null if the
	 * 		permissions should be determined by the umask.
	 * @throws FileNotFoundException if the file does not exist and was not to be created.
	 * @throws FileException if the file cannot be opened.
	 */
	public function __construct($fileName, $mode, $permissions = null) {
		if ($permissions !== null) {
			$umask = new TemporaryUmask($permissions ^ 0777);
		}
		$this->_fileName = $fileName;
		if ($mode[0] === 'c' && Php::isPhpAtLeastVersion(5, 2, 6) == false) {
			$mode[0] = 'r';
			$this->_handle = fopen($fileName, $mode);
			if ($this->_handle === false) {
				$mode[0] = 'w';
				$this->_handle = fopen($fileName, $mode);
			}
		} else {
			$this->_handle = fopen($fileName, $mode);
		}
		if ($this->_handle === false) {
			if ($mode[0] === 'r') {
				throw new FileNotFoundException($fileName);
			} else {
				throw new FileException("Cannot open or create " . $fileName);
			}

		}
	}
	
	public function __destruct() {
		if ($this->_isLocked) {
			flock($this->_handle, LOCK_UN);
		}
		fclose($this->_handle);
	}
	
	public function seek($offset, $whence = SEEK_SET) {
		$result = fseek($this->_handle, $offset, $whence);
		if ($result !== 0) {
			throw new FileException("Couldn't seek " . $this->_fileName . ".");
		}
	}
	
	public function tell() {
		$pos = ftell($this->_handle);
		if ($pos === false) {
			throw new FileException("Couldn't tell position of " . $this->_fileName . ".");
		}
		return $pos;
	}

	public function read($length) {
		if ($length === 0) {
			return '';
		}
		$content = fread($this->_handle, $length);
		if ($content === false) {
			throw new FileException("Couldn't read " . $this->_fileName . ".");
		}
		return $content;
	}

	public function readExactly($length) {
		$content = $this->read($length);
		if (strlen($content) !== $length) {
			throw new FileException("Did not read expected length from file.");
		}
		return $content;
	}

	public function readToEnd() {
		$fileSize = $this->getSize();
		$pos = $this->tell();
		return $this->read($fileSize - $pos);
	}

	public function readAll() {
		$fileSize = $this->getSize();
		$this->tell(0);
		return $this->read($fileSize);
	}

	public function write($data) {
		if (empty($data)) {
			return 0;
		}
		$result = fwrite($this->_handle, $data);
		if ($result === false) {
			throw new FileException("Couldn't write to " . $this->_fileName . ".");
		}
		return $result;
	}

	public function writeAll($data) {
		$numberOfBytesWritten = $this->write($data);
		if ($numberOfBytesWritten !== strlen($data)) {
			throw new FileException("Couldn't write all bytes to " . $this->_fileName . ".");
		}
	}

	public function skip($size) {
		$startPos = $this->tell();
		$fileSize = $this->getSize();
		if ($startPos > $fileSize) {
			return 0;
		}
		$targetPos = min($fileSize, $startPos + $size);
		$this->seek($targetPos, SEEK_SET);
		$endPos = $this->tell();
		if ($endPos < $startPos) {
			throw new FileException("Failed to skip bytes of " . $this->_fileName . ".");
		}
		return $endPos - $startPos;
	}

	public function skipExactly($size) {
		$bytesSkipped = $this->skip($size);
		if ($bytesSkipped !== $size) {
			throw new FileException("Failed to skip " . $size . " bytes of " . $this->_fileName . ".");
		}
	}

	public function lock($operation, &$wouldblock = null, $timeout = self::LOCAL_LOCK_TIMEOUT) {
		if ($operation === LOCK_UN) {
			$this->unlock($wouldblock);
			return;
		}
		try {
			$this->_localLock = new LocalLock($this->_getLocalLockName(), $timeout);
		} catch (\Exception $e) {
			throw new FileException("Couldn't locally lock " . $this->_fileName . ".");
		}
		$result = flock($this->_handle, $operation, $wouldblock);
		if ($result === false) {
			throw new FileException("Couldn't lock " . $this->_fileName . ".");
		}
		$this->_isLocked = true;
	}

	public function unlock(&$wouldblock = null) {
		if ($this->_isLocked) {
			$this->_localLock = null;
			$result = flock($this->_handle, LOCK_UN, $wouldblock);
			if ($result === false) {
				throw new FileException("Couldn't unlock " . $this->_fileName . ".");
			}
			$this->_isLocked = false;
		}
	}
	
	public function truncate($size) {
		$result = ftruncate ($this->_handle, $size);
		if ($result === false) {
			throw new FileException("Couldn't truncate " . $this->_fileName . ".");
		}
	}
	
	public function getSize() {
		$pos = $this->tell();
		$this->seek(0, SEEK_END);
		$size = $this->tell();
		$this->seek($pos);
		return $size;
	}
	
	public function getFilename() {
		return $this->_fileName;
	}
	
	public function passThru() {
		$result = fpassthru($this->_handle);
		if ($result === false) {
			throw new FileException("Couldn't pass through " . $this->_fileName . ".");
		}
	}

	public function writeRepeatedByte($byte, $numberOfBytes) {
		$totalBytesWritten = 0;
		if ($numberOfBytes >= 1024*100) {
			$string = str_repeat(chr($byte), 1024*100);
			while ($numberOfBytes >= 1024*100) {
				$bytesWritten = $this->write($string);
				$totalBytesWritten += $bytesWritten;
				if ($bytesWritten < 1024*100) {
					return $totalBytesWritten;
				}
				$numberOfBytes -= 1024*100;
			}
		}
		if ($numberOfBytes) {
			$string = str_repeat(chr($byte), $numberOfBytes);
			$totalBytesWritten += $this->write($string);
		}
		return $totalBytesWritten;
	}

	public function writeAllRepeatedBytes($byte, $numberOfBytes) {
		$numberOfBytesWritten = $this->writeRepeatedByte($byte, $numberOfBytes);
		if ($numberOfBytesWritten !== strlen($numberOfBytes)) {
			throw new FileException("Couldn't write all bytes to " . $this->_fileName . ".");
		}
	}

	public function flush() {
		$result = fflush($this->_handle);
		if ($result === false) {
			throw new FileException("Can't flush file " . $this->_fileName . ".");
		}
	}

	public function getModifyTime() {
		$modifyTime = filemtime($this->_fileName);
		if ($modifyTime === false) {
			throw new FileException("Can't find modify time of " . $this->_fileName . ".");
		}
		return $modifyTime;
	}

	public function unlink() {
		$result = unlink($this->_fileName);
		if ($result === false) {
			throw new FileException("Can't delete " . $this->_fileName . ".");
		}
	}

	public function getRealPath() {
		if ($this->_realPath === null) {
			$this->_realPath = realpath($this->_fileName);
			if ($this->_realPath === false) {
				throw new FileException("Can't get real path of " . $this->_fileName);
			}
		}
		return $this->_realPath;
	}

	public function chmod($mode) {
		$result = chmod($this->_fileName, $mode);
		if ($result === false) {
			throw new FileException("Can't chmod " . $this->_fileName . ' to 0' . sprintf('%o', $mode));
		}
	}

	public function fileperms() {
		$perms = fileperms($this->_fileName);
		if ($perms === false) {
			throw new FileException("Can't get file permissions of " . $this->_fileName);
		}
		return $perms;
	}

	public function stat() {
		$result = fstat($this->_handle);
		if ($result === false) {
			throw new FileException("Can't stat " . $this->_fileName);
		}
		return $result;
	}

	/**
	 * @return resource
	 */
	public function getHandle() {
		return $this->_handle;
	}

	/**
	 * @param $newPath
	 * @throws \Exception
	 */
	public function rename($newPath) {
		$result = rename($this->_fileName, $newPath);
		if ($result === false) {
			throw new \Exception("Can't rename " . $this->_fileName . ' to ' . $newPath);
		}
		$this->_fileName = $newPath;
	}

	private function _getLocalLockName() {
		$stat = $this->stat();
		return 'filelock:' . $stat['dev'] . ':' . $stat['ino'];
	}

}