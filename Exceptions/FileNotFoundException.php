<?php
namespace GorillaHub\FilesBundle\Exceptions;

/**
 * @package GorillaHub/FilesBundle
 */
class FileNotFoundException extends \Exception {
	public function __construct($fileName = null) {
		parent::__construct("File not found" . ($fileName !== null ? ": " . $fileName : ""));
	}
}