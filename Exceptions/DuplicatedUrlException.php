<?php

namespace GorillaHub\FilesBundle\Exceptions;

/**
 * Class DuplicatedUrlException
 * @package GorillaHub\FilesBundle\Exceptions
 */
class DuplicatedUrlException extends FileException
{

}