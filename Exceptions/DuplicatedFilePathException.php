<?php

namespace GorillaHub\FilesBundle\Exceptions;

/**
 * @package GorillaHub\FilesBundle\Exceptions
 */
class DuplicatedFilePathException extends FileException
{

}