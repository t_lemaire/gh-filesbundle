<?php
namespace GorillaHub\FilesBundle;

/**
 * @ignore This class is internal to this bundle.
 * @package GorillaHub/FilesBundle
 *
 * Since MemoryFile is only a handle to a memory file, and multiple handles may exist to the actual memory file,
 * an object of this class represents the actual memory file to which the handles refer.
 */
class ActualMemoryFile
{
	/** @var string The contents of the file. */
	public $data;

	/** @var string The name of the file. */
	public $fileName;

	/** @var int Time at which the data was last modified. */
	public $modifyTime;

	/** @var int The file mode. */
	public $mode = 0777;

	public function __construct($data = '', $fileName = 'memory_file') {
		$this->data = $data;
		$this->fileName = $fileName;
		$this->modifyTime = time();
		$this->mode = 0777 ^ umask();
	}

}