<?php
namespace GorillaHub\FilesBundle;

use \GorillaHub\FilesBundle\Exceptions\FileException;
use \GorillaHub\FilesBundle\Exceptions\FileNotFoundException;

/**
 * @package GorillaHub/FilesBundle
 *
 * An object of this class represents the filesystem.  The methods in this class can be used instead of the built-in
 * PHP functions for the following benefits:
 *
 * 		1) These functions may be more convenient;
 * 		2) These functions throw exceptions, sparing the caller from always checking return values; and
 * 		3) If an object of this type is injected, it can be substituted with a mock filesystem.
 */
class FileSystem extends AbstractFileSystem
{
	public function createDirectory($directory, $permissions = 0777)
	{
		if (is_dir($directory) === false) {
			$temporaryUmask = new TemporaryUmask(0);
			if ($permissions !== null) {
				$readPermissions = $permissions & 0444;
				$dirPermissions = $permissions | ($readPermissions >> 2);
				@mkdir($directory, $dirPermissions, true);
			} else {
				exec('mkdir -p ' . escapeshellarg($directory));
			}
			if (is_dir($directory) === false) {
				throw new \Exception("Can't create directory " . $directory);
			}
		}
	}

	public function getFileSize($path) {
		$size = filesize($path);
		if (!is_int($size)) {
			throw new FileNotFoundException($path);
		}
		return $size;
	}

	public function isFile($path) {
		return is_file($path);
	}

	public function chmod($path, $mode) {
		$result = chmod($path, $mode);
		if ($result === false) {
			throw new FileException("Can't change file permissions of $path to " . sprintf("%o", $mode) . ".");
		}
	}

	public function filePerms($path) {
		$perms = file_perms($path);
		if (!is_int($perms)) {
			throw new FileException("Can't get permissions of : $path");
		}
		return $perms;
	}

	public function filePutContents($path, $data, $flags = 0) {
		$result = file_put_contents($path, $data, $flags);
		if ($result !== strlen($data)) {
			throw new FileException("Cannot write to: " . $path);
		}
	}

	public function fileGetContents(
		$path,
		$useIncludePath = false,
		$context = null,
		$offset = -1,
		$maxLen = null
	) {
		if ($maxLen !== null) {
			$result = file_get_contents($path, $useIncludePath, $context, $offset, $maxLen);
		} else {
			$result = file_get_contents($path, $useIncludePath, $context, $offset);
		}
		if ($result === false) {
			throw new FileException("Could not read: " . $path);
		}
		return $result;
	}

	public function createSafeName($name) {
		return urlencode($name);
	}
}
