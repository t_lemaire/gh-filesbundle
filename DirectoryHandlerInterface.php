<?php

namespace GorillaHub\FilesBundle;

use GorillaHub\FilesBundle\Domain\Directory;

interface DirectoryHandlerInterface
{

	public function getList(Directory $directory);

} 