<?php

namespace GorillaHub\FilesBundle\Tests;

use \GorillaHub\FilesBundle\Paths;

/**
 * @package GorillaHub\FilesBundle
 * @ignore This class is internal to this bundle.
 */
class PathsTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @dataProvider cleanPathProvider
	 */
	public function testCleanPath($expected, $in) {

		$this->assertSame($expected, Paths::cleanPath($in));
	}

	public function cleanPathProvider() {

		return array(
			array('', ''),
			array('hi', 'hi'),
			array('/hi', '/hi'),
			array('/hi/', '/hi/'),
			array('hi/', 'hi/'),
			array('', 'hi/..'),
			array('/', '/hi/..'),
			array('', 'hi/../'),
			array('/', '/hi/../'),
			array('', './'),
			array('', '.'),
			array('', '..'),
			array('dir1/dir3', 'dir1/dir2/../dir3'),
			array('dir1/dir3', '../..//dir1/dir2/../dir3')
		);
	}

	public function testAddTrailingSlash() {
		$this->assertSame('/', Paths::addTrailingSlash(''));
		$this->assertSame('/', Paths::addTrailingSlash('/'));
		$this->assertSame('/test/', Paths::addTrailingSlash('/test'));
		$this->assertSame('/test/', Paths::addTrailingSlash('/test/'));
	}

	public function testRemoveTrailingSlash() {
		$this->assertSame('', Paths::removeTrailingSlash(''));
		$this->assertSame('', Paths::removeTrailingSlash('/'));
		$this->assertSame('/test', Paths::removeTrailingSlash('/test'));
		$this->assertSame('/test', Paths::removeTrailingSlash('/test/'));
	}

	public function testAddLeadingSlash() {
		$this->assertSame('/', Paths::addLeadingSlash(''));
		$this->assertSame('/', Paths::addLeadingSlash('/'));
		$this->assertSame('/test', Paths::addLeadingSlash('test'));
		$this->assertSame('/test/', Paths::addLeadingSlash('/test/'));
	}

	public function testRemoveLeadingSlash() {
		$this->assertSame('', Paths::removeLeadingSlash(''));
		$this->assertSame('', Paths::removeLeadingSlash('/'));
		$this->assertSame('test/', Paths::removeLeadingSlash('test/'));
		$this->assertSame('test/', Paths::removeLeadingSlash('/test/'));
	}

	/**
	 * @dataProvider joinPathsProvider
	 */
	public function testJoinPaths($expected, $input) {
		$this->assertSame($expected, call_user_func_array(array('\GorillaHub\FilesBundle\Paths', 'joinPaths'), $input));
	}

	public function joinPathsProvider() {
		return array(
			array("/absolute/path", array('/absolute/', 'path')),
			array("/absolute/path", array('/absolute', 'path')),
			array("/absolute/path", array('/absolute/', '/path')),
			array("/absolute/path", array('/absolute', '/path')),
			array("/absolute/path/", array('/absolute/', '/path/')),
			array("relative/path", array('relative/', '/path')),
			array("three/part/path", array('three', 'part', 'path')),
			array("three/part/path", array('three/', 'part', '/path')),
			array("/three/part/path/", array('/three/', '/part', 'path/')),
			array("/this/path/has/six/different/parts", array('/this', 'path/', 'has', '/six/', '/different', 'parts'))
		);
	}

	/**
	 * @dataProvider replacePathRootProvider
	 */
	public function testReplacePathRoot($expected, $fullPath, $oldRoot, $newRoot) {
		$this->assertSame($expected, Paths::replacePathRoot($fullPath, $oldRoot, $newRoot));
	}

	public function replacePathRootProvider() {
		return array(
			array('/e/f/c/d.txt', '/a/b/c/d.txt', '/a/b', '/e/f'),
			array('e/f/c/d.txt', '/a/b/c/d.txt', '/a/b', 'e/f'),
			array('e/', '/a/b/', '/a/b', 'e'),
			array('e', '/a/b', '/a/b', 'e')
		);
	}

	/**
	 * @dataProvider getExtensionFromPathProvider
	 */
	public function testGetExtensionFromPath($expected, $input) {
		$this->assertSame($expected, Paths::getExtensionFromPath($input));
	}

	public function getExtensionFromPathProvider() {
		return array(
			array('wmv', 'hi.wmv'),
			array('', 'hi.'),
			array(null, 'hi')
		);
	}

	/**
	 * @dataProvider expandTildeToHomeProvider
	 */
	public function testExpandTildeToHome($expected, $input)
	{
		$this->assertSame($expected, Paths::expandTildeToHome($input));
	}

	public function expandTildeToHomeProvider()
	{
		$home = getenv('HOME');

		return array(
			array('/mats', '/mats'),
			array('hats', 'hats'),
			array($home, '~'),
			array($home . '/', '~/'),
			array($home . '/cats', '~/cats'),
			array('~bats', '~bats'),
			array('/~/bats', '/~/bats')
		);
	}
}
